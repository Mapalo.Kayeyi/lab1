package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {

            System.out.println("Let's play round " + roundCounter);

            String humanChoice = userChoice("Your choice (Rock/Paper/Scissors)?", rpsChoices);

            String computerChoice = computerChoices();

            if (isWinner(humanChoice, computerChoice)) {
                System.out
                        .println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore++;

            } else if (isWinner(computerChoice, humanChoice)) {

                System.out.println(
                        "Human chose " + humanChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore++;

            }

            else {

                System.out
                        .println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". I'ts a tie!");

            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continueAnswer = userChoice(("Do you wish to continue playing? (y/n)?"), Arrays.asList("y", "n"));

            if (continueAnswer.equals("n")) {
                System.out.println("Bye bye :)");
                break;

            }

            roundCounter++;
        }

    }

    // Fra forelsning
    public boolean isWinner(String Choice1, String Choice2) {
        if (Choice1.equals("rock")) {
            return Choice2.equals("scissors");
        } else if (Choice1.equals("paper")) {
            return Choice2.equals("rock");
        } else {
            return Choice2.equals("paper");
        }

    }

    public String computerChoices() {
        Random random = new Random();
        int randomIndex = random.nextInt(rpsChoices.size());
        return rpsChoices.get(randomIndex);
    }

    public String userChoice(String prompt, List<String> validInput) {

        String userChoice;

        while (true) {
            userChoice = readInput(prompt);
            if (validateInput(userChoice, validInput)) {
                break;
            } else {
                System.out.println("I do not understand " + userChoice + ". Could you try again?");
            }

        }
        return userChoice;
    }

    public boolean validateInput(String input, List<String> validInput) {
        return validInput.contains(input);
    }

 
    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
